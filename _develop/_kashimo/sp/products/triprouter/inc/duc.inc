<h3><span>Triprouter</span>の特徴</h3>
  <div class="tab_ct">
    <h4 class="show_sp">USB接続型で、1台3役をこなす万能端末！</h4>
    <dl>
      <dt class="show_pc">USB接続型で、1台3役をこなす万能端末！</dt>
      <dd>Triprouterは、モバイルWi-Fiルーター、モバイルバッテリー、USB接続データカードの1台3役でご利用できます。複数のデバイスを持ち歩くのが面倒、という方向けの端末です。普段充電器を持ちながらルーターも併用して持ち運びしている方にとっては、Triprouterを持っていることで、1台3役を担えるためおすすめです。</p></dd>
    </dl>
    <picture>
      <source media="(max-width: 768px)" srcset="images/img_tab01_sp.png" />
      <img src="images/img_tab01.png" alt="Price info" />
    </picture>
  </div>
  <div class="tab_ct">
    <h4 class="show_sp">他の端末を充電できる<br class="show_pc">モバイルバッテリー機能搭載</h4>
    <dl>
      <dt class="show_pc">他の端末を充電できる<br>モバイルバッテリー機能搭載</dt>
      <dd>Triprouterから他の端末にケーブルを繋げば、スマホやタブレットを充電することもできます。Triprouterのバッテリー容量は2,500mAhで、大容量のバッテリーを搭載しているスマホでもフル充電できます。</dd>
    </dl>
    <picture>
      <source media="(max-width: 768px)" srcset="images/img_tab02_sp.png" />
      <img src="images/img_tab02.png" alt="Price info" />
    </picture>
  </div>