<h3><span>Speed Wi-Fi HOME L01S</span>のスペック</h3>
<table class="tbl">
  <tbody>
    <tr>
      <th>製品名</th>
      <td>Speed Wi-Fi NEXT W05</td>
    </tr>
    <tr>
      <th>製造元</th>
      <td>HUAWEI</td>
    </tr>
    <tr>
      <th>対応ネットワーク</th>
      <td>
        ・WiMAX 2+<br>・au 4G LTE※2
      </td>
    </tr>
    <tr>
      <th>質量</th>
      <td>約131g</td>
    </tr>
    <tr>
      <th>同梱物</th>
      <td>
        ・USB2.0 TypeC-A変換ケーブル（試供品）<br>・取扱説明書<br>・保証書
      </td>
    </tr>
    <tr>
      <th>外形寸法（mm）</th>
      <td>約W130×H55×D12.6mm</td>
    </tr>
    <tr>
      <th>UIMカードバージョン※3</th>
      <td>Nano IC</td>
    </tr>
    <tr>
      <th>バッテリー使用時間※4</th>
      <td>・連続通信時間（ハイスピードモード：WiMAX 2+）<br>・ノーマルモード：約540分<br>・連続待受<br>・クイックアクセスモードOFF時：850時間</td>
    </tr>
    <tr>
      <th>対応OS※5</th>
      <td>Windows 10、Windows 8.1、Windows 7<br>Mac OS X v10.6、v10.7、v10.8、v10.9、v10.10、v10.11、v10.12、v10.13</td>
    </tr>
    <tr>
      <th>Wi-Fi規格</th>
      <td>IEEE802.11a/b/g/n/ac(2.4GHz/5GHz対応)</td>
    </tr>
    <tr>
      <th>アクセサリ（別売り）</th>
      <td>クレードル</td>
    </tr>
    <tr>
      <th>Body-SAR値</th>
      <td>0.986W/kg</td>
    </tr>
    <tr>
      <th>本体色</th>
      <td>・ブラック×ライム<br>・ホワイト×シルバー</td>
    </tr>
  </tbody>
</table>
<ul class="list">
  <li>★&nbsp;表記の通信速度はシステム上の最大速度となります。通信速度は、通信環境やネットワークの混雑状況などに応じて異なります</li>
  <li>
    &nbsp;au 4G LTEのネットワークは、エリアにより最大通信速度が異なります。
  </li>
  <li>&nbsp;旧機種と新機種でUIMカードバージョンの異なる機種変更を行う場合、端末到着後、「回線切替サイト」での登録が必要です。</li>
  <li>&nbsp;実際にご利用になる使用環境や電波状況により異なります。</li>
  <li>&nbsp;2017年4月現在、Microsoft社のサポートが終了したOSは、対応OSから対象外とさせて頂きます。</li>
</ul>