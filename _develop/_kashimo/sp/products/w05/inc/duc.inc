<h3><span>Speed Wi-Fi NEXT W05</span>の特徴</h3>
  <div class="tab_ct">
    <h4 class="show_sp">下り最大758Mbps、上りも最大112.5Mpbsを実現し、ネット通信の高速化を実現！</h4>
    <dl>
      <dt class="show_pc">下り最大758Mbps、上りも最大112.5Mpbsを実現し、ネット通信の高速化を実現！</dt>
      <dd>
        WIMAXで最速のルーターです。<br>下り458Mbpsのスピードは、4×4MINOに対応し高速通信が可能となりました。
        <span>表記の速度はシステム上の下り最大速度であり、ご利用機器や提供エリアの対応状況により若干の差異が出ることをご了承くださいませ。実際の通信速度は通信環境や混雑状況に応じて異なります。<br><br class="show_sp">※1ハイスピードプラスエリアモードを設定した月は別途LTEオプション料1,005円/月（税抜）がかかります。</span>
      </dd>
    </dl>
    <picture>
      <source media="(max-width: 768px)" srcset="images/img_tab01_sp.png" />
      <img src="images/img_tab01.png" alt="Price info" />
    </picture>
  </div>
  <div class="tab_ct">
    <h4 class="show_sp">スマホやパソコン側の再設定が<br>不要になる、WiFiお引越し機能が搭載！</h4>
    <dl>
      <dt class="show_pc">スマホやパソコン側の再設定が<br class="show_pc">不要になる、WiFiお引越し機能が搭載！</dt>
      <dd>新しいルーター端末にした場合、使うデバイス毎にWi-Fi設定のし直しが必要です。その手間がなくなる、wiﬁお引越し機能搭載端末です。Wi-Fi接続情報（SSID、暗号化モード、暗号化キー）が引き継げて 、再設定の必要がなくなります。<br class="show_pc">手間がかかる面倒な再設定がなくなり、使いやすい端末です。</dd>
    </dl>
    <picture>
      <source media="(max-width: 768px)" srcset="images/img_tab02_sp.png" />
      <img src="images/img_tab02.png" alt="Price info" />
    </picture>
  </div>
  <div class="tab_ct">
    <h4 class="show_sp">シンプルなデザインで、軽量化した端末！</h4>
    <dl>
      <dt class="show_pc">シンプルなデザインで、軽量化した端末！</dt>
      <dd>旧端末のW04よりも、角ばったデザインと薄さでよりコンパクトなタイプのルーターです。重さもW04の140gと比較すると、W05は約131gと軽量化しております。<br class="show_pc">より軽くなったことで、持ち運びができ、外出先でも使いやすくおすすめです。</dd>
    </dl>
    <picture>
      <source media="(max-width: 768px)" srcset="images/img_tab03_sp.png" />
      <img src="images/img_tab03.png" alt="Price info" />
    </picture>
  </div>