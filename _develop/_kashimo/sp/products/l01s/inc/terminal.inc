<section class="st_terminal">
  <div class="row">
    <h3>その他の端末ラインナップ</h3>
    <div class="gr_art">
      <article>
        <figure class="img_w_05"><img src="../../../products/images/pr_01_sp.png" alt="img w05"></figure>
        <div class="main_art">
          <em>スタンダード・持ち運び型</em>
          <h4>Speed Wi-Fi NEXT<strong>W05</strong></h4>
          <p>
            <span class="txt_f">下り最大</span>
            <span class="txt_m">758<small>Mbps</small></span>
            <ins>※4</ins>
          </p>
          <a href="/sp/products/w05" class="link">W05の詳細をみる</a>
        </div>
        <!--/.main_art-->
      </article> 
      <article>
        <figure class="img_tri"><img src="../../../products/images/pr_03_sp.png" alt="img tri"></figure>
        <div class="main_art">
          <em>USB接続型</em>
          <h4><strong>Triprouter</strong></h4>
          <p>
            <span class="txt_f">下り最大</span>
            <span class="txt_m">220<small>Mbps</small></span>
            <ins>※4</ins>
          </p>
          <a href="/sp/products/triprouter" class="link">Triprouterの詳細をみる</a>
        </div>
        <!--/.main_art-->
      </article> 
    </div>
    <!--/.gr_art-->
  </div>
</section>
<!--/.st_terminal-->