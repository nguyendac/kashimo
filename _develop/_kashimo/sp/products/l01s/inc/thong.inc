<h3><span>Speed Wi-Fi HOME L01S</span>のスペック</h3>
<table class="tbl">
  <tbody>
    <tr>
      <th>製品名</th>
      <td>Speed Wi-Fi HOME L01/L01s</td>
    </tr>
    <tr>
      <th>製造元</th>
      <td>HUAWEI</td>
    </tr>
    <tr>
      <th>対応ネットワーク</th>
      <td>
        ・WiMAX 2+<br>
        ・au 4G LTE※2
      </td>
    </tr>
    <tr>
      <th>質量</th>
      <td>L01 約493g / L01s 約450g</td>
    </tr>
    <tr>
      <th>同梱物</th>
      <td>
        ・ACアダプタ<br>
        ・Ethernetケーブル（試供品）<br>
        ・取扱説明書<br>
        ・保証書
      </td>
    </tr>
    <tr>
      <th>外形寸法（mm）</th>
      <td>約H180×W93×D93mm</td>
    </tr>
    <tr>
      <th>UIMカードバージョン※3</th>
      <td>L01 Micro ICカード / L01s Nano ICカード</td>
    </tr>
    <tr>
      <th>対応OS※5</th>
      <td>
        Windows® 10、8.1、7<br>Mac OS X v10.13、v10.12、v10.11、v10.10、v10.9、v10.8、v10.7、v10.6、v10.5
      </td>
    </tr>
    <tr>
      <th>有線LAN規格</th>
      <td>Ethernet 1000Base-T/100Base-TX/10Base-T</td>
    </tr>
    <tr>
      <th>無線LAN規格</th>
      <td>IEEE802.11ac/n/a（5GHz帯）、11n/g/b（2.4GHz帯） ※2.4GHz/5GHzは同時利用可能</td>
    </tr>
    <tr>
      <th>最大同時接続数</th>
      <td>計42台（LANポート×2台、Wi-Fi SSID×2×20台</td>
    </tr>
    <tr>
      <th>電源</th>
      <td>・電源はACアダプタのみ<br>・バッテリー動作非対応</td>
    </tr>
    <tr>
      <th>本体色</th>
      <td>ホワイト</td>
    </tr>
  </tbody>
</table>
<ul class="list">
  <li>
    &nbsp;下り最大440Mbpsは順次全国へ拡大中です。上記の速度はシステム上の下り最大速度であり、ご利用機器や提供エリアの対応状況により、下り最大220Mbpsまたは110Mbpsとなります。<br>
    440Mbps対応地域の詳細はサービスエリアマップを参照ください。
  </li>
  <li>&nbsp;au 4G LTEのネットワークは、エリアにより最大通信速度が異なります。</li>
  <li>&nbsp;旧機種と新機種でUIMカードバージョンの異なる機種変更を行う場合、製品（端末）到着後、「回線切替サイト」での登録が必要です。</li>
  <li>&nbsp;2017年4月現在、Microsoft社のサポートが終了したOSは、対応OSから対象外とさせて頂きます。</li>
</ul>