<h3><span>Speed Wi-Fi HOME L01S</span>の特徴</h3>
  <div class="tab_ct">
    <h4 class="show_sp">工事不要、自宅で利用に便利！</h4>
    <dl>
      <dt class="show_pc">工事不要、自宅で利用に便利！</dt>
      <dd>ご契約後に端末が自宅に到着して、工事不要でご利用いただけます。<br class="show_pc">「外出先などでは使わないけど、室内で使いたい！」という自宅利用に適したモデルなので、自宅のみで利用されたい方におすすめな端末です。</p></dd>
    </dl>
    <picture>
      <source media="(max-width: 768px)" srcset="images/img_tab01_sp.png" />
      <img src="images/img_tab01.png" alt="Price info" />
    </picture>
  </div>
  <div class="tab_ct">
    <h4 class="show_sp">同時接続42台まで可能！</h4>
    <dl>
      <dt class="show_pc">同時接続42台まで可能！</dt>
      <dd>Speed Wi-Fi HOME L01Sは、最大同時接続が42台まで可能な端末です。<br class="show_pc">室内据え置きタイプなので、Wi-Fiが届く範囲も広くなっており、室内全体をカバーできる端末です。様々な機器との通信が可能で、他の機器と干渉が少なく、スムーズな通信環境をご利用いただけます。</dd>
    </dl>
    <picture>
      <source media="(max-width: 768px)" srcset="images/img_tab02_sp.png" />
      <img src="images/img_tab02.png" alt="Price info" />
    </picture>
  </div>