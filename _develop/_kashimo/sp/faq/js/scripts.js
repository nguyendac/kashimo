window.addEventListener('DOMContentLoaded',function(){
  new Faq();
})
var Faq = (function(){
  function Faq(){
    var q = this;
    this.target = document.querySelectorAll('.main_art dt');
    /*this.obj = this.target.querySelectorAll('dt');*/
    Array.prototype.forEach.call(q.target,function(item){
      item.addEventListener('click',function(e){
        if(item.classList.contains('active')){
          item.classList.remove('active');
          item.nextElementSibling.style.height = 0;
        } else {
          item.classList.add('active');
          item.nextElementSibling.style.height = item.nextElementSibling.children[0].clientHeight+'px';
        }
      })
    })
    window.addEventListener('resize',function(){
      Array.prototype.forEach.call(q.target,function(item){
        if(item.classList.contains('active')){
          item.classList.add('active');
          item.nextElementSibling.style.height = item.nextElementSibling.children[0].clientHeight+'px';
        }
      })
    })
  }
  return Faq;
})()