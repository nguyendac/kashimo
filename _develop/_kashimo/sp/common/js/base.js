window.requestAnimFrame = (function(callback) {
  return window.requestAnimationFrame ||
  window.webkitRequestAnimationFrame ||
  window.mozRequestAnimationFrame ||
  window.oRequestAnimationFrame ||
  window.msRequestAnimationFrame ||
  function(callback){
    return window.setTimeout(callback, 1000/60);
  };
})();

window.cancelAnimFrame = (function(_id) {
  return window.cancelAnimationFrame ||
  window.cancelRequestAnimationFrame ||
  window.webkitCancelAnimationFrame ||
  window.webkitCancelRequestAnimationFrame ||
  window.mozCancelAnimationFrame ||
  window.mozCancelRequestAnimationFrame ||
  window.msCancelAnimationFrame ||
  window.msCancelRequestAnimationFrame ||
  window.oCancelAnimationFrame ||
  window.oCancelRequestAnimationFrame ||
  function(_id) { window.clearTimeout(id); };
})();
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    __.prototype = b.prototype;
    d.prototype = new __();
};
function closest(el, selector) {
  // type el -> Object
  // type select -> String
  var matchesFn;
  // find vendor prefix
  ['matches', 'webkitMatchesSelector', 'mozMatchesSelector', 'msMatchesSelector', 'oMatchesSelector'].some(function(fn) {
    if (typeof document.body[fn] == 'function') {
      matchesFn = fn;
      return true;
    }
    return false;
  })
  var parent;
  // traverse parents
  while (el) {
    parent = el.parentElement;
    if (parent && parent[matchesFn](selector)) {
      return parent;
    }
    el = parent;
  }
  return null;
}

function getCssProperty(elem, property) {
  return window.getComputedStyle(elem, null).getPropertyValue(property);
}
var transform = ["transform", "msTransform", "webkitTransform", "mozTransform", "oTransform"];
var flex = ['-webkit-box', '-moz-box', '-ms-flexbox', '-webkit-flex', 'flex'];
var fd = ['flexDirection', '-webkit-flexDirection', '-moz-flexDirection'];
var animatriondelay = ["animationDelay","-moz-animationDelay","-wekit-animationDelay"];
function getSupportedPropertyName(properties) {
  for (var i = 0; i < properties.length; i++) {
    if (typeof document.body.style[properties[i]] != "undefined") {
      return properties[i];
    }
  }
  return null;
}
var transformProperty = getSupportedPropertyName(transform);
var flexProperty = getSupportedPropertyName(flex);
var fdProperty = getSupportedPropertyName(fd);
var ad = getSupportedPropertyName(animatriondelay);
var easingEquations = {
  easeOutSine: function(pos) {
    return Math.sin(pos * (Math.PI / 2));
  },
  easeInOutSine: function(pos) {
    return (-0.5 * (Math.cos(Math.PI * pos) - 1));
  },
  easeInOutQuint: function(pos) {
    if ((pos /= 0.5) < 1) {
      return 0.5 * Math.pow(pos, 5);
    }
    return 0.5 * (Math.pow((pos - 2), 5) + 2);
  }
};

function isPartiallyVisible(el) {
  var elementBoundary = el.getBoundingClientRect();
  var top = elementBoundary.top;
  var bottom = elementBoundary.bottom;
  var height = elementBoundary.height;
  return ((top + height >= 0) && (height + window.innerHeight >= bottom));
}

function isFullyVisible(el) {
  var elementBoundary = el.getBoundingClientRect();
  var top = elementBoundary.top;
  var bottom = elementBoundary.bottom;
  return ((top >= 0) && (bottom <= window.innerHeight));
}

function CreateElementWithClass(elementName, className) {
  var el = document.createElement(elementName);
  el.className = className;
  return el;
}

function createElementWithId(elementName, idName) {
  var el = document.createElement(elementName);
  el.id = idName;
  return el;
}

function getScrollbarWidth() {
  var outer = document.createElement("div");
  outer.style.visibility = "hidden";
  outer.style.width = "100px";
  document.body.appendChild(outer);
  var widthNoScroll = outer.offsetWidth;
  // force scrollbars
  outer.style.overflow = "scroll";
  // add innerdiv
  var inner = document.createElement("div");
  inner.style.width = "100%";
  outer.appendChild(inner);
  var widthWithScroll = inner.offsetWidth;
  // remove divs
  outer.parentNode.removeChild(outer);
  return widthNoScroll - widthWithScroll;
}

function insertAfter(referenceNode, newNode) {
  referenceNode.parentNode.insertBefore(newNode, referenceNode.nextSibling);
}
var wordsToArray = function wordsToArray(str) {return str.split('').map(function (e) {return e === ' ' ? '&nbsp;' : e;});};

function insertSpan(elem, letters, startTime) {
  elem.textContent = ''
  var curr = 0
  var delay = 20
  letters.forEach(function(letter,i){
    var span = document.createElement('span');
    span.classList.add('waveText');
    span.innerHTML = letter
    span.style[ad] = (++curr / delay + (startTime || 0)) + 's'
    elem.appendChild(span)
  })
}
function getPosition(el) {
  var xPos = 0;
  var yPos = 0;
  while (el) {
    if (el.tagName == "BODY") {
      // deal with browser quirks with body/window/document and page scroll
      var xScroll = el.scrollLeft || document.documentElement.scrollLeft;
      var yScroll = el.scrollTop || document.documentElement.scrollTop;

      xPos += (el.offsetLeft - xScroll + el.clientLeft);
      yPos += (el.offsetTop - yScroll + el.clientTop);
    } else {
      // for all other non-BODY elements
      xPos += (el.offsetLeft - el.scrollLeft + el.clientLeft);
      yPos += (el.offsetTop - el.scrollTop + el.clientTop);
    }
    el = el.offsetParent;
  }
  return {
    x: xPos,
    y: yPos
  };
}
// (function () {
//   var PicturePolyfill = (function () {
//     function PicturePolyfill() {
//       var _this = this;
//       this.pictures = [];
//       this.onResize = function () {
//         var width = document.body.clientWidth;
//         for (var i = 0; i < _this.pictures.length; i = (i + 1)) {
//           _this.pictures[i].update(width);
//         };
//       };
//       if ([].includes) return;
//       var picture = Array.prototype.slice.call(document.getElementsByTagName('picture'));
//       for (var i = 0; i < picture.length; i = (i + 1)) {
//         this.pictures.push(new Picture(picture[i]));
//       };
//       window.addEventListener("resize", this.onResize, false);
//       this.onResize();
//     }
//     return PicturePolyfill;
//   }());
//   var Picture = (function () {
//     function Picture(node) {
//       var _this = this;
//       this.update = function (width) {
//         width <= _this.breakPoint ? _this.toSP() : _this.toPC();
//       };
//       this.toSP = function () {
//         if (_this.isSP) return;
//         _this.isSP = true;
//         _this.changeSrc();
//       };
//       this.toPC = function () {
//         if (!_this.isSP) return;
//         _this.isSP = false;
//         _this.changeSrc();
//       };
//       this.changeSrc = function () {
//         var toSrc = _this.isSP ? _this.srcSP : _this.srcPC;
//         _this.img.setAttribute('src', toSrc);
//       };
//       this.img = node.getElementsByTagName('img')[0];
//       this.srcPC = this.img.getAttribute('src');
//       var source = node.getElementsByTagName('source')[0];
//       this.srcSP = source.getAttribute('srcset');
//       this.breakPoint = Number(source.getAttribute('media').match(/(\d+)px/)[1]);
//       this.isSP = !document.body.clientWidth <= this.breakPoint;
//       this.update();
//     }
//     return Picture;
//   }());
//   new PicturePolyfill();
// }());
window.addEventListener('DOMContentLoaded', function() {
  new Anchor();
  new Scroll();
  new MenuSp();
  new Subnav();
  new Fsc();
});
var Anchor = (function() {
  function Anchor() {
    var a = this;
    this._target = '.anchor';
    this._header = document.getElementById('header');
    this._icon_nav = document.getElementById('nav_ham');
    this._nav = document.getElementById('nav');
    this.timer;
    this.flag_start = false;
    this.iteration;
    this.eles = document.querySelectorAll(this._target);
    this.stopEverything = function() { a.flag_start = false; }
    this._getbuffer = function() {
      var _buffer;
      if(window.innerWidth < 769) {
        _buffer = a._header.clientHeight;
      } else {
        _buffer = a._header.clientHeight;
      }
      return _buffer;
    }
    
    this.scrollToY = function(scrollTargetY, speed, easing) {
      var scrollY = window.scrollY || window.pageYOffset,
        scrollTargetY = scrollTargetY || 0,
        speed = speed || 2000,
        easing = easing || 'easeOutSine',
        currentTime = 0;
      var time = Math.max(.1, Math.min(Math.abs(scrollY - scrollTargetY) / speed, .8));

      function tick() {
        if (a.flag_start) {
          currentTime += 1 / 60;
          var p = currentTime / time;
          var t = easingEquations[easing](p);
          if (p < 1) {
            requestAnimFrame(tick);
            window.scrollTo(0, scrollY + ((scrollTargetY - scrollY) * t));
          } else { window.scrollTo(0, scrollTargetY); }
        }
      }
      tick();
    }
    Array.prototype.forEach.call(this.eles, function(el, i) {
      el.addEventListener('click', function(e) {
        var next = el.getAttribute('href').split('#')[1];
        if (document.getElementById(next)) {
          a.flag_start = true;
          e.preventDefault();
          a.scrollToY((document.getElementById(next).offsetTop - a._getbuffer() - 20), 1500, 'easeOutSine');
          if (window.innerWidth < 769) {
            a._icon_nav.classList.remove('open');
            a._nav.classList.remove('open');
            document.body.style.overflow = "inherit";
          }
        }
      })
    });
    this._start = function() {
      var next = window.location.hash.split('#')[1];
      a.flag_start = true;
      if (next) { a.scrollToY((document.getElementById(next).offsetTop - a._getbuffer() - 20), 1500, 'easeOutSine'); }
    }
    window.addEventListener('load', a._start, false);
    document.querySelector("body").addEventListener('mousewheel', a.stopEverything, false);
    document.querySelector("body").addEventListener('DOMMouseScroll', a.stopEverything, false);
  }
  return Anchor;
})();
var Scroll = (function(){
  function Scroll(){
    var s = this;
    this._target = document.getElementById('header_scroll');
    this._header = document.getElementById('header');
    this._flg = this._header.clientHeight;
    this._for_sp = function(top){
      document.body.style.paddingTop = s._header.clientHeight+'px';
      if(top >= 0) {
        s._header.classList.add('fixed');
      } else {
        s._header.classList.remove('fixed');
      }
    }
    this.handling = function(){
      var _top  = document.documentElement.scrollTop || document.body.scrollTop;
      if(_top > s._flg) {
        s._target.classList.add('active');
      } else {
        s._target.classList.remove('active');
      }
      if(window.innerWidth < 769) {
        s._for_sp(_top);
      } else {
        document.body.style.paddingTop = 0;
        s._header.classList.remove('fixed');
        document.body.style.overflow = 'inherit';
      }
    }
    window.addEventListener('scroll',s.handling,false);
    window.addEventListener('resize',s.handling,false);
    window.addEventListener('load',s.handling,false);
  }
  return Scroll;
})()
var MenuSp = (function(){
  function MenuSp(){
    var m = this;
    this._target = document.getElementById('open_nav');
    this._close = document.getElementById('close_nav');
    this._mobile = document.getElementById('menu');
    this._header = document.getElementById('header');
    this._overlay = document.getElementById('overlay');
    this._target.addEventListener('click',function(e){
      e.preventDefault();
      if(this.classList.contains('open')){
        this.classList.remove('open');
        m._mobile.classList.remove('open');
        m._overlay.classList.remove('open');
        document.body.style.overflow = 'inherit';
      } else {
        this.classList.add('open');
        m._mobile.classList.add('open');
        m._overlay.classList.add('open');
        document.body.style.overflow = 'hidden';
      }
    })
    this._close.addEventListener('click',function(e){
      e.preventDefault();
      m._target.click();
    })
    m._overlay.addEventListener("click", function(){
      m._target.click();
    })
  }
  return MenuSp;
})()
var Subnav = (function(){
  function Subnav(){
    var s = this;
    this._targets =  document.querySelectorAll('.accord');
    Array.prototype.forEach.call(s._targets,function(el){
      el.addEventListener('click',function(e){
        e.preventDefault();
        var _next =  el.nextElementSibling;
        if(el.classList.contains('open')) {
          el.classList.remove('open');
          _next.style.height = 0;
        } else {
          el.classList.add('open')
          var _h = 0;
          Array.prototype.forEach.call(_next.children,function(c,i){
            _h+= c.clientHeight;
          })
          _next.style.height = _h+"px";
        }
      })
    })
    this._reset = function(){
      if(window.innerWidth < 769) {
        Array.prototype.forEach.call(s._targets,function(el) {
          if(el.classList.contains('open')) {
            var _h = 0;
            var _next =  el.nextElementSibling;
            Array.prototype.forEach.call(_next.children,function(c,i){
              if(i > 0) {
                _h+= c.clientHeight;
              }
            })
            _next.style.height = _h+"px";
          }
        })
      } else {
        Array.prototype.forEach.call(s._targets,function(el){
          el.classList.remove('open');
          el.nextElementSibling.removeAttribute('style');
        })
      }
    }
    // window.addEventListener('resize',s._reset,false);
  }
  return Subnav;
})()
var Fsc = (function(){
  function Fsc(){
    var f = this;
    this._fmid = document.getElementById('fmid');
    this._target = document.getElementById('fsc');
    this._footer = document.getElementById('footer');
    this._start = 50;
    this.handling = function(){
      var _top = window.pageYOffset;
      if(_top > f._start) {
        f._target.classList.add('fixed');
        f._footer.style.paddingTop = f._target.clientHeight+'px';
      } else {
        f._target.classList.remove('fixed');
        f._footer.style.paddingTop = 0;
      }
      if(isPartiallyVisible(f._fmid)) {
        f._footer.style.paddingTop = 0;
        f._target.classList.remove('fixed');
      }
      if(window.innerWidth > 768) {
        f._footer.style.paddingTop = 0;
      }
    }
    window.addEventListener('resize',f.handling,false);
    window.addEventListener('scroll',f.handling,false);
    window.addEventListener('load',f.handling,false);
  }
  return Fsc;
})()