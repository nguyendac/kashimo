<div class="footer_top df jc_c" id="fsc">
  <div class="btn_wimax_order">
    <a href="/sp/wimax/order" data-txt="簡単！">
      <span class="df ai_c">今すぐ3分でWebお申し込み</span>
    </a>
  </div>
</div>
<div class="footer_mid" id="fmid">
  <div class="footer_mid_main df jc_sb row">
    <dl>
      <dt>サービス</dt>
      <dd>
        <ul>
          <li><a href="/sp/">トップページ</a></li>
          <li class="show_pc"><a href="/sp/about">wimaxとは</a></li>
          <li class="show_pc"><a href="/sp/area">対応エリア確認</a></li>
          <li class="show_pc"><a href="/sp/faq">よくある質問</a></li>
        </ul>
      </dd>
    </dl>
    <dl>
      <dt>プラン</dt>
      <dd>
        <ul>
          <li>
            <a href="/sp/plan">WiMAX料金プラン一覧</a><span class="accord">control</span>
            <ul>
              <li><a href="/sp/plan/gigahodai">ギガ放題プラン</a></li>
              <li><a href="/sp/plan/light">ライトプラン</a></li>
            </ul>
          </li>
        </ul>
      </dd>
    </dl>
    <dl>
      <dt>端末</dt>
      <dd>
        <ul>
          <li>
            <a href="/sp/products">WiMAX製品一覧</a><span class="accord">control</span>
            <ul>
              <li><a href="/sp/products/w05">W05</a></li>
              <li><a href="/sp/products/l01s">L01S</a></li>
              <li><a href="/sp/products/triprouter">Triprouter</a></li>
            </ul>
          </li>
        </ul>
      </dd>
    </dl>
    <dl>
      <dt>お申込み・お問い合わせ</dt>
      <dd>
        <ul>
          <li class="show_sp"><a href="/sp/area">対応エリア確認</a></li>
          <li class="show_sp"><a href="/sp/faq">よくある質問</a></li>
          <li><a href="/sp/wimax/order">申し込み</a></li>
          <li><a href="/sp/contact">お問い合わせ</a></li>
          <li><a href="/sp/contact?if_company=true">法人様お問い合わせ</a></li>
        </ul>
      </dd>
    </dl>
  </div>
</div>
<div class="footer_bot">
  <div class="footer_bot_logo">
    <div class="footer_bot_logo_r df ai_c row">
      <a href="/sp/"><img src="/common/images/logo_footer.png" alt="kashimo"></a>
      <ul class="df ai_c">
        <li><a href="/sp/company">会社概要</a></li>
        <li><a href="/sp/pdf/terms.pdf">利用規約</a></li>
        <li><a href="/sp/pdf/instructions.pdf">重要事項説明</a></li>
        <li><a href="/sp/privacy">プライバシーポリシー</a></li>
        <li><a href="/sp/law">特定商取引に基づく表示</a></li>
      </ul>
    </div>
  </div>
  <div class="footer_bot_copy">東京都公安委員会許可 第307731606438号 © 2016 MEmobile Co., Ltd.</div>
</div>
<div id="overlay" class="overlay"></div>