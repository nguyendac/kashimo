<div class="header_main row">
  <div class="header_top df jc_sb ai_c">
    <h1><a href="/sp/"><img src="/common/images/logo.png" alt="wimax"></a></h1>
    <div class="header_top_btn df show_pc">
      <a href="/sp/contact" class="_envelop"><span class="df jc_c ai_c">お問い合わせ</span></a>
      <a href="/sp/wimax/order" class="_lap"><span class="df jc_c ai_c">今すぐWebでお申し込み</span></a>
    </div>
    <div class="header_top_r show_sp">
      <a href="/sp#" class="apply">お申し込み</a>
      <a href="#" id="open_nav" class="menu_bar">MENU</a>
    </div>
  </div>
  <div class="header_bottom" id="menu">
    <nav class="header_nav">
      <p class="show_sp"><span class="close_nav" id="close_nav">閉じる</span></p>
      <!--#include virtual="/sp/common/include/nav.inc"-->
      <div class="header_nav_btn show_sp">
        <a href="/sp/contact" class="_envelop"><span class="df jc_c ai_c">お問い合わせ</span></a>
        <a href="/sp/wimax/order" class="_lap"><span class="df jc_c ai_c">今すぐWebでお申し込み</span></a>
      </div>
    </nav>
    <div class="header_nav_sc show_pc" id="header_scroll">
      <div class="header_nav_sc_row row df jc_sb ai_c">
        <a href="/sp/"><img src="/common/images/logo.png" alt="wimax"></a>
        <!--#include virtual="/sp/common/include/nav.inc"-->
        <a href="/sp/wimax/order"><span class="df jc_c ai_c">今すぐWebでお申し込み</span></a>
      </div>
    </div>
  </div>
</div>