<ul class="header_nav_l df">
  <li>
    <a href="/sp/products" class="icon_wimax">WiMAX製品一覧</a>
    <span class="accord">arrow</span>
    <ul>
      <li><a href="/sp/products">WiMAX製品一覧</a></li>
      <li><a href="/sp/products/w05">Speed Wi-Fi NEXT W05</a></li>
      <li><a href="/sp/products/l01s">Speed Wi-Fi HOME L01S</a></li>
      <li><a href="/sp/products/triprouter">Triprouter</a></li>
    </ul>
  </li>
  <li>
    <a href="/sp/plan" class="icon_yen">WiMAX料金プラン一覧</a>
    <span class="accord">arrow</span>
    <ul>
      <li><a href="/sp/plan">WiMAXプラン一覧</a></li>
      <li><a href="/sp/plan/gigahodai">ギガ放題プラン</a></li>
      <li><a href="/sp/plan/light">ライトプラン</a></li>
    </ul>
  </li>
  <li><a href="/sp/about" class="icon_ques">WiMAXとは</a></li>
  <li><a href="/sp/area" class="icon_location">対応エリア</a></li>
  <li><a href="/sp/faq" class="icon_faq">よくある質問</a></li>
</ul>