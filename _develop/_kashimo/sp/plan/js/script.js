window.addEventListener('DOMContentLoaded',function(){
  new init();
  new Popup();
})

var tabLinks = new Array();
var contentDivs = new Array();

function init() {
  var tabListItems = document.getElementById('tabs').childNodes;
  for ( var i = 0; i < tabListItems.length; i++ ) {
    if ( tabListItems[i].nodeName == "LI" ) {
      var tabLink = getFirstChildWithTagName( tabListItems[i], 'A' );
      var id = getHash( tabLink.getAttribute('href') );
      tabLinks[id] = tabLink;
      contentDivs[id] = document.getElementById( id );
    }
  }
  var i = 0;
  for ( var id in tabLinks ) {
    tabLinks[id].onclick = showTab;
    tabLinks[id].onfocus = function() { this.blur() };
    if ( i == 0 ) tabLinks[id].className = 'selected';
    i++;
  }
  var i = 0;
  for ( var id in contentDivs ) {
    if ( i != 0 ) contentDivs[id].className = 'tabContent hide';
    i++;
  }
}

function showTab() {
  var selectedId = getHash( this.getAttribute('href') );
  for ( var id in contentDivs ) {
    if ( id == selectedId ) {
      tabLinks[id].className = 'selected';
      contentDivs[id].className = 'tabContent';
    } else {
      tabLinks[id].className = '';
      contentDivs[id].className = 'tabContent hide';
    }
  }
  return false;
}

function getFirstChildWithTagName( element, tagName ) {
  for ( var i = 0; i < element.childNodes.length; i++ ) {
    if ( element.childNodes[i].nodeName == tagName ) return element.childNodes[i];
  }
}

function getHash( url ) {
  var hashPos = url.lastIndexOf ( '#' );
  return url.substring( hashPos + 1 );
}

var Popup = (function(){
  function Popup(){
    var p = this;
    this._target = document.getElementById('popup');
    this._content =  document.getElementById('content_popup');
    this._events = document.getElementById('price_table');
    Array.prototype.forEach.call(p._events.querySelectorAll('.btn_pop'),function(el){
      el.addEventListener('click',function(e){
        e.preventDefault();
        p._target.classList.add('open');
        document.body.style.overflow = 'hidden';
      })
    })
    this._target.querySelector('.close').addEventListener('click',function(e){
      e.preventDefault();
      p._target.classList.remove('open');
      document.body.style.overflow = 'inherit';
    })
  }
  return Popup;
})()