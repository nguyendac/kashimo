// 呼び出し先（エリアマップ）
var areaMapUrl = "https://www13.info-mapping.com/uq/sm/map.aspx?M=011";
// 呼び出し先（ピンポイント判定）
var pinpointUrl = "https://www13.info-mapping.com/uq/sm/sim.aspx?M=011&C=1";
function openAreaMapGPS() {
    if (navigator.geolocation) {
        // 現在の位置情報取得を実施
        navigator.geolocation.getCurrentPosition(
        // 位置情報取得成功時
        function (pos) { 
            var form = document.createElement("form");
            document.body.appendChild(form);
            var input_cx = document.createElement("input");
            var input_cy = document.createElement("input");
            var input_cz = document.createElement("input");
            input_cx.setAttribute("type","hidden");
            input_cy.setAttribute("type","hidden");
            input_cz.setAttribute("type","hidden");
            input_cx.setAttribute("name","cx");
            input_cy.setAttribute("name","cy");
            input_cz.setAttribute("name","cz");
            input_cx.setAttribute("value",pos.coords.longitude);
            input_cy.setAttribute("value",pos.coords.latitude);
            input_cz.setAttribute("value","3");
            form.appendChild(input_cx);
            form.appendChild(input_cy);
            form.appendChild(input_cz);
            form.setAttribute("action",areaMapUrl);
            form.setAttribute("method","post");
            form.submit();
        },
        // 位置情報取得失敗時
        function (pos) {
                window.alert("位置情報が取得できませんでした");
        },
        {
            timeout : 10000,
            enableHighAccuracy: true
        });
    } else {
        window.alert("本ブラウザでは位置が取得できません");
    }
}
function openPinpointGPS() {
    if (navigator.geolocation) {
        // 現在の位置情報取得を実施
        navigator.geolocation.getCurrentPosition(
        // 位置情報取得成功時
        function (pos) { 
            var form = document.createElement("form");
            document.body.appendChild(form);
            var input_mx = document.createElement("input");
            var input_my = document.createElement("input");
            input_mx.setAttribute("type","hidden");
            input_my.setAttribute("type","hidden");
            input_mx.setAttribute("name","mx");
            input_my.setAttribute("name","my");
            input_mx.setAttribute("value",pos.coords.longitude);
            input_my.setAttribute("value",pos.coords.latitude);
            form.appendChild(input_mx);
            form.appendChild(input_my);
            form.setAttribute("action",pinpointUrl);
            form.setAttribute("method","post");
            form.submit();
        },
        // 位置情報取得失敗時
        function (pos) {
                window.alert("位置情報が取得できませんでした");
        },
        {
            timeout : 10000,
            enableHighAccuracy: true
        });
    } else {
        window.alert("本ブラウザでは位置が取得できません");
    }
}