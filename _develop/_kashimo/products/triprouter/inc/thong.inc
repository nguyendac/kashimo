<h3><span>Triprouter</span>のスペック</h3>
<table class="tbl">
  <tbody>
    <tr>
      <th>製品名</th>
      <td>Triprouter</td>
    </tr>
    <tr>
      <th>製造元</th>
      <td>株式会社ジェネタス</td>
    </tr>
    <tr>
      <th>対応ネットワーク</th>
      <td>
        ・WiMAX 2+<br>・au 4G LTE※2
      </td>
    </tr>
    <tr>
      <th>質量</th>
      <td>約100g（Speed USB STICK U01と合わせて約138g）</td>
    </tr>
    <tr>
      <th>同梱物</th>
      <td>
        ・Triprouter 本体<br>・ACアダプタ<br>・microUSBケーブル （充電・データ転送ケーブル）<br>・USBコネクタマーキングシール<br>・ご利用にあたっての注意事項（保証書付）<br>・クイックスタートガイド
      </td>
    </tr>
    <tr>
      <th>外形寸法（mm）</th>
      <td>W108×H62×D24.5mm</td>
    </tr>
    <tr>
      <th>バッテリー使用時間※4</th>
      <td>・約6時間（連続通信時間）※1<br>・約15時間（WiFi繋がず、待機しているだけの時間＝連続待受時間）※1</td>
    </tr>
    <tr>
      <th>対応OS※5</th>
      <td>MacOS X 10.7～10.12, Windows10(32/64bit), Windows8.1(32/64bit), <br>Windows8(32/64bit), Windows7(32/64bit), iOS6～10,  Android4～6</td>
    </tr>
    <tr>
      <th>Wi-Fi規格</th>
      <td>IEEE802.11b</td>
    </tr>
    <tr>
      <th>アクセサリ（別売り）</th>
      <td>クレードル</td>
    </tr>
    <tr>
      <th>本体色</th>
      <td>ホワイト×シルバー</td>
    </tr>
  </tbody>
</table>
<ul class="list">
  <li>★&nbsp;&nbsp;表記の通信速度はシステム上の最大速度となります。通信速度は、通信環境やネットワークの混雑状況などに応じて異なります</li>
  <li>※2&nbsp;&nbsp;au 4G LTEのネットワークは、エリアにより最大通信速度が異なります。</li>
  <li>※4&nbsp;&nbsp;実際にご利用になる使用環境や電波状況により異なります。</li>
  <li>※5&nbsp;&nbsp;2017年4月現在、Microsoft社のサポートが終了したOSは、対応OSから対象外とさせて頂きます。</li>
</ul>