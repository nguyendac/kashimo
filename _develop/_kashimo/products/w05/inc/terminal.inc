<section class="st_terminal">
  <div class="row">
    <h3>その他の端末ラインナップ</h3>
    <div class="gr_art">
      <article>
        <figure class="img_l_01"><img src="../../../products/images/pr_02_sp.png" alt="img w05"></figure>
        <div class="main_art">
          <em>据え置き型・室内利用向け</em>
          <h4>Speed Wi-Fi HOME<strong>L01S</strong></h4>
          <p>
            <span class="txt_f">下り最大</span>
            <span class="txt_m">440<small>Mbps</small></span>
            <ins>※4</ins>
          </p>
          <a href="/products/l01s" class="link">L01Sの詳細を見る</a>
        </div>
        <!--/.main_art-->
      </article> 
      <article>
        <figure class="img_tri"><img src="../../../products/images/pr_03_sp.png" alt="img tri"></figure>
        <div class="main_art">
          <em>USB接続型</em>
          <h4><strong>Triprouter</strong></h4>
          <p>
            <span class="txt_f">下り最大</span>
            <span class="txt_m">220<small>Mbps</small></span>
            <ins>※4</ins>
          </p>
          <a href="/products/triprouter" class="link">Triprouterの詳細をみる</a>
        </div>
        <!--/.main_art-->
      </article> 
    </div>
    <!--/.gr_art-->
  </div>
</section>
<!--/.st_terminal-->