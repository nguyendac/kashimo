window.addEventListener('DOMContentLoaded',function(){
  new init();
  $('.slider-for').slick({
     slidesToShow: 1,
     slidesToScroll: 1,
     arrows: false,
     fade: false,
     asNavFor: '.slider-nav',
  });
  $('.slider-nav').slick({
   slidesToShow: 5,
   slidesToScroll: 1,
   asNavFor: '.slider-for',
   arrows: false,
   focusOnSelect: true,
   variableWidth: true,
  });
  new Control();
})
var Control = (function(){
  function Control(){
    var c = this;
    this._target = document.getElementById('control');
    this._colors = this._target.querySelectorAll('li');
    this._cname = this._colors[0].dataset.color;
    this.gallery = document.getElementById('gallery');
    this.gallery.dataset.color = c._cname;
    this._colors[0].classList.add('active');
    Array.prototype.forEach.call(this._colors,function(el){
      el.addEventListener('click',function(e){
        Array.prototype.forEach.call(c._colors,function(a) {
          a.classList.remove('active');
        })
        this.classList.add('active');
        c._cname = el.dataset.color;
        c.gallery.dataset.color = c._cname;
      })
    })
  }
  return Control;
})()
var tabLinks = new Array();
var contentDivs = new Array();

function init() {
  var tabListItems = document.getElementById('tabs').childNodes;
  for ( var i = 0; i < tabListItems.length; i++ ) {
    if ( tabListItems[i].nodeName == "LI" ) {
      var tabLink = getFirstChildWithTagName( tabListItems[i], 'A' );
      var id = getHash( tabLink.getAttribute('href') );
      tabLinks[id] = tabLink;
      contentDivs[id] = document.getElementById( id );
    }
  }
  var i = 0;
  for ( var id in tabLinks ) {
    tabLinks[id].onclick = showTab;
    tabLinks[id].onfocus = function() { this.blur() };
    if ( i == 0 ) tabLinks[id].className = 'selected';
    i++;
  }
  var i = 0;
  for ( var id in contentDivs ) {
    if ( i != 0 ) contentDivs[id].className = 'tabContent hide';
    i++;
  }
}

function showTab() {
  var selectedId = getHash( this.getAttribute('href') );
  for ( var id in contentDivs ) {
    if ( id == selectedId ) {
      tabLinks[id].className = 'selected';
      contentDivs[id].className = 'tabContent';
    } else {
      tabLinks[id].className = '';
      contentDivs[id].className = 'tabContent hide';
    }
  }
  return false;
}

function getFirstChildWithTagName( element, tagName ) {
  for ( var i = 0; i < element.childNodes.length; i++ ) {
    if ( element.childNodes[i].nodeName == tagName ) return element.childNodes[i];
  }
}

function getHash( url ) {
  var hashPos = url.lastIndexOf ( '#' );
  return url.substring( hashPos + 1 );
}