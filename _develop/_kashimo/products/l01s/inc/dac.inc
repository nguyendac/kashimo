<div id="detail" class="detail">
  <div class="detail_wrap row">
    <div class="detail_main">
      <div class="detail_left gallery" id="gallery">
        <div class="slider slider-for gallery_large">
          <figure><img src="images/detail_01.png" alt="detail"></figure>
          <figure><img src="images/detail_02.png" alt="detail"></figure>
          <figure><img src="images/detail_03.png" alt="detail"></figure>
          <figure><img src="images/detail_04.png" alt="detail"></figure>
          <figure><img src="images/detail_05.png" alt="detail"></figure>
        </div>
        <div class="gallery_control">
          <ul id="control">
            <li data-color="color_01"><img src="images/color_01.png" alt="color"></li>
          </ul>
          <div class="slider slider-nav gallery_thumb">
            <figure><img src="images/detail_01.png" alt="detail"></figure>
            <figure><img src="images/detail_02.png" alt="detail"></figure>
            <figure><img src="images/detail_03.png" alt="detail"></figure>
            <figure><img src="images/detail_04.png" alt="detail"></figure>
            <figure><img src="images/detail_05.png" alt="detail"></figure>
          </div>
        </div>
      </div>
      <div class="detail_right">
        <div class="detail_right_ttl">
          <h4>Speed Wi-Fi HOME <span>L01S</span></h4>
          <p>同時接続42台まで可能！室内据え置きタイプ</p>
        </div>
        <div class="corr_area">
          <span class="txt_corr"><ins>対応エリア</ins></span>
          <ul class="l_logo">
            <li><img src="/products/images/wimax_2plus.png" alt="Wimax"></li>
            <li><img src="/products/images/4g.png" alt="4g"></li>
          </ul>
          <p><a href="/area">対応エリア確認をする</a></p>
        </div>
        <ul class="_info">
          <li data-before="下り最大" data-after="Mbps" class="speed">440</li>
          <li class="quality" data-after="質量"><span>約450g</span></li>
          <li class="bluetooth" data-after="bluetooth"><span>対応</span></li>
          <li class="pin" data-after="同時接続"><span>42台</span></li>
          <li class="signal" data-after="連続通信"><span>約600分</span></li>
        </ul>
        <div class="order">
          <span>端末代金<br>0 円</span>
          <a href="/wimax/order?device=L01S" class="bx_shadown_btn">この端末でお申し込みする</a>
        </div>
      </div>
    </div>
    <ul class="breadcrumb show_pc">
      <li><a href="/">トップ</a></li>
      <li><a href="/products">端末一覧</a></li>
      <li>Speed Wi-Fi HOME L01S</li>
    </ul>
  </div>
</div>